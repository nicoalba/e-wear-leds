[1mdiff --git a/Ewear/Ewear.ino b/Ewear/Ewear.ino[m
[1mindex 453996c..06a26b7 100644[m
[1m--- a/Ewear/Ewear.ino[m
[1m+++ b/Ewear/Ewear.ino[m
[36m@@ -23,24 +23,26 @@[m [mboolean B_set = false;[m
 // ------------------------------------------------------------------[m
 // MODE STUFF     MODE STUFF     MODE STUFF     MODE STUFF  [m
 // ------------------------------------------------------------------[m
[31m-int mode = 5;[m
[32m+[m[32mint mode = 1;[m
 int num_modes = 5;[m
 [m
 // Defined Color schemes (num Modes)[m
 CRGB rainbow[num];[m
 CRGB flameRed[num];[m
 CRGB lovePink[num];[m
[32m+[m[32mCRGB prettyPurple[num];[m
[32m+[m[32mCRGB oceanBlue[num];[m
 [m
 [m
 //Undefined Color schemes[m
[31m-CRGB deepBlue[num];[m
[32m+[m[32m//CRGB deepBlue[num];[m
 CRGB alienGreen[num];[m
[31m-CRGB prettyPurple[num];[m
 CRGB sunYellow[num];[m
 CRGB greebBlue[num];[m
[31m-CRGB oceanBlue[num];[m
 [m
 [m
[32m+[m[32mint sound = A2;[m
[32m+[m
 // ------------------------------------------------------------------[m
 // SETUP    SETUP    SETUP    SETUP    SETUP    SETUP    SETUP[m
 // ------------------------------------------------------------------[m
[36m@@ -50,7 +52,7 @@[m [mCRGB leds[num];  // Define the array of leds to control[m
 void setup() { [m
   [m
   [m
[31m-  // Serial.begin (9600); // -> This is useful for debugging if you need to display things on a screen. [m
[32m+[m[32m  Serial.begin (9600); // -> This is useful for debugging if you need to display things on a screen.[m[41m [m
   pinMode(encoderPinA, INPUT_PULLUP); // new method of enabling pullups[m
   pinMode(encoderPinB, INPUT_PULLUP); // Don't really understand this[m
   pinMode(modeButton, INPUT_PULLUP);  // Still don't understand this[m
[36m@@ -66,6 +68,7 @@[m [mvoid setup() {[m
   FastLED.setBrightness(255*encoderPos/maxEncoder);     // Set brigtness to medium[m
   FastLED.show();                                       // Show [m
 [m
[32m+[m[32m  pinMode(sound, INPUT);[m
   [m
 }[m
 [m
[36m@@ -74,6 +77,19 @@[m [mvoid setup() {[m
 // ------------------------------------------------------------------[m
 [m
 void loop() { [m
[32m+[m
[32m+[m
[32m+[m
[32m+[m[32m  int volume = analogRead(sound);[m
[32m+[m[32m  Serial.println(volume);[m
[32m+[m[32m  delay(50);[m
[32m+[m
[32m+[m[41m  [m
[32m+[m
[32m+[m
[32m+[m
[32m+[m
[32m+[m[41m  [m
   // FOLLOWING LINE WAS FROM EXAMPLE BUT I DON'T UNDERSTAND [m
   rotating = true;  // reset the debouncer[m
   [m
[36m@@ -151,20 +167,16 @@[m [mvoid setLovePink(){[m
   lovePink[3] = CRGB(255,20,147);  lovePink[4] = CRGB(255,20,147);    lovePink[5] = CRGB(255,20,147);[m
   lovePink[6] = CRGB(230,190,190); [m
 }[m
[31m-[m
 void setPrettyPurple(){[m
   prettyPurple[0] = CRGB(37.5,0,65);  prettyPurple[1] = CRGB(106/2,77/2,255/2);  prettyPurple[2] = CRGB(34/2,0,204/2);[m
   prettyPurple[3] = CRGB(37.5,0,65);  prettyPurple[4] = CRGB(106/2,77/2,255/2);    prettyPurple[5] = CRGB(37.5,0,65);[m
   prettyPurple[6] = CRGB(34/2,0,204/2); [m
 }[m
[31m-[m
 void setOceanBlue(){[m
   oceanBlue[0] = CRGB(25/2,255/2,255/2);  oceanBlue[1] = CRGB(0,128/2,153/2);  oceanBlue[2] = CRGB(0,30/2,179/2);[m
   oceanBlue[3] = CRGB(153/2,238/2,255/2);  oceanBlue[4] = CRGB(0,30/2,179/2);   oceanBlue[5] = CRGB(0,128/2,153/2);[m
   oceanBlue[6] = CRGB(25/2,255/2,255/2); [m
 }[m
[31m-[m
[31m-[m
 void changeMode(int n){[m
   switch(n){[m
     case 1: setLEDs(rainbow); break;[m
