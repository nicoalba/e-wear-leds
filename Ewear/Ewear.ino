#include <FastLED.h>

// ------------------------------------------------------------------
// PIN ASSIGNMENTS    PIN ASSIGNMENTS    PIN ASSIGNMENTS  
// ------------------------------------------------------------------
#define num 7             // num LEDs
#define DATA_PIN 13       // Control Pin: LEDs
#define encoderPinA 2   // right (labeled DT on our decoder)
#define encoderPinB 3   // left (labeled CLK on our decoder)
#define modeButton 4    // switch (labeled SW on our decoder)

// ------------------------------------------------------------------
// ENCODER STUFF        ENCODER STUFF        ENCODER STUFF 
// ------------------------------------------------------------------
static unsigned int maxEncoder = 50;
volatile unsigned int encoderPos = maxEncoder/2;  // a counter for the dial
unsigned int lastReportedPos = maxEncoder/2;      // change management
static boolean rotating=false;                    // debounce management
// interrupt service routine vars
boolean A_set = false;
boolean B_set = false;

// ------------------------------------------------------------------
// MODE STUFF     MODE STUFF     MODE STUFF     MODE STUFF  
// ------------------------------------------------------------------
int mode = 1;
int num_modes = 5;

// Defined Color schemes (num Modes)
CRGB rainbow[num];
CRGB flameRed[num];
CRGB lovePink[num];
CRGB prettyPurple[num];
CRGB oceanBlue[num];


//Undefined Color schemes
//CRGB deepBlue[num];
CRGB alienGreen[num];
CRGB sunYellow[num];
CRGB greebBlue[num];


int sound = A2;

// ------------------------------------------------------------------
// SETUP    SETUP    SETUP    SETUP    SETUP    SETUP    SETUP
// ------------------------------------------------------------------

CRGB leds[num];  // Define the array of leds to control

void setup() { 
  
  
  Serial.begin (9600); // -> This is useful for debugging if you need to display things on a screen. 
  pinMode(encoderPinA, INPUT_PULLUP); // new method of enabling pullups
  pinMode(encoderPinB, INPUT_PULLUP); // Don't really understand this
  pinMode(modeButton, INPUT_PULLUP);  // Still don't understand this
  attachInterrupt(0, doEncoderA, CHANGE); // encoder pin on interrupt bitwise 0 (pin 2)
  attachInterrupt(1, doEncoderB, CHANGE); // encoder pin on interrupt bitwise 1 (pin 3)

  
  initializeModes();                                    // Initialize mode colors
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, num);   // Instantiate LED instance
  changeMode(mode);                                     // Change mode to 1

  
  FastLED.setBrightness(255*encoderPos/maxEncoder);     // Set brigtness to medium
  FastLED.show();                                       // Show 

  pinMode(sound, INPUT);
  
}

// ------------------------------------------------------------------
// LOOP     LOOP     LOOP     LOOP     LOOP     LOOOP     LOOP
// ------------------------------------------------------------------

void loop() { 



  int volume = analogRead(sound);
  Serial.println(volume);
  delay(50);

  




  
  // FOLLOWING LINE WAS FROM EXAMPLE BUT I DON'T UNDERSTAND 
  rotating = true;  // reset the debouncer
  
  if (digitalRead(modeButton) == LOW )  { //If button is being pushed, change mode
    if(mode < num_modes){ mode ++;}
    else mode = 1;      // Wrap mode back to 1
    changeMode(mode);
    delay(500);         // Delay so mode doesn't change to quick
  }
  

}

// This was copied and modified from the internet
// Interrupt on A changing state
void doEncoderA(){
  // debounce
  if ( rotating ) delay (1);  // wait a little until the bouncing is done
  // Test transition, did things really change?
  if( digitalRead(encoderPinA) != A_set ) {  // debounce once more
    A_set = !A_set;
    // adjust counter + if A leads B
    if ( A_set && !B_set ){
      if(encoderPos < maxEncoder){ // If encoder is already MAX, don't increase more
        encoderPos += 1; 
        FastLED.setBrightness(255*encoderPos/maxEncoder); // Normalize brighness
        FastLED.show();
        //Serial.println(encoderPos); // -> Uncomment if need to debug encoder
      }
    }
   
    rotating = false;  // no more debouncing until loop() hits again
  }
}
// This was copied and modified from the internet
// Interrupt on B changing state, same as A above
void doEncoderB(){
  if ( rotating ) delay (1);
  if( digitalRead(encoderPinB) != B_set ) {
    B_set = !B_set;
    //  adjust counter – 1 if B leads A
    if( B_set && !A_set ){
      if(encoderPos > 0){ // If encoder is already 0, don't decrease more
        encoderPos -= 1;     
        FastLED.setBrightness(255*encoderPos/maxEncoder);  // Normalize brighness
        FastLED.show();
        // Serial.println(encoderPos); // -> Uncomment if need to debug encoder
      }
    }
    rotating = false;
  }
}

void initializeModes(){
  setRainbow();
  setFlameRed();
  setLovePink();
//  setDeepBlue(); 
  setPrettyPurple();
  setOceanBlue();
}

void setRainbow(){
  rainbow[0] = CRGB(255,0,0);  rainbow[1] = CRGB(230,40,0);  rainbow[2] = CRGB(200,120,0);
  rainbow[3] = CRGB(0,255,0);  rainbow[4] = CRGB(0,0,255);    rainbow[5] = CRGB(40,0,255);
  rainbow[6] = CRGB(150,0,255); 
}
void setFlameRed(){
  flameRed[0] = CRGB(230,0,20);  flameRed[1] = CRGB(255,0,0);  flameRed[2] = CRGB(230,40,0);
  flameRed[3] = CRGB(230,0,20);  flameRed[4] = CRGB(255,0,0);  flameRed[5] = CRGB(230,40,0);
  flameRed[6] = CRGB(230,190,190); 
}
void setLovePink(){
  lovePink[0] = CRGB(255,20,147);  lovePink[1] = CRGB(255,20,147);  lovePink[2] = CRGB(255,20,147);
  lovePink[3] = CRGB(255,20,147);  lovePink[4] = CRGB(255,20,147);    lovePink[5] = CRGB(255,20,147);
  lovePink[6] = CRGB(230,190,190); 
}
void setPrettyPurple(){
  prettyPurple[0] = CRGB(37.5,0,65);  prettyPurple[1] = CRGB(106/2,77/2,255/2);  prettyPurple[2] = CRGB(34/2,0,204/2);
  prettyPurple[3] = CRGB(37.5,0,65);  prettyPurple[4] = CRGB(106/2,77/2,255/2);    prettyPurple[5] = CRGB(37.5,0,65);
  prettyPurple[6] = CRGB(34/2,0,204/2); 
}
void setOceanBlue(){
  oceanBlue[0] = CRGB(25/2,255/2,255/2);  oceanBlue[1] = CRGB(0,128/2,153/2);  oceanBlue[2] = CRGB(0,30/2,179/2);
  oceanBlue[3] = CRGB(153/2,238/2,255/2);  oceanBlue[4] = CRGB(0,30/2,179/2);   oceanBlue[5] = CRGB(0,128/2,153/2);
  oceanBlue[6] = CRGB(25/2,255/2,255/2); 
}
void changeMode(int n){
  switch(n){
    case 1: setLEDs(rainbow); break;
    case 2: setLEDs(flameRed); break;
    case 3: setLEDs(lovePink); break;  
    case 4: setLEDs(prettyPurple);break;
    case 5: setLEDs(oceanBlue);break;
    }
  FastLED.show();
}
void setLEDs(CRGB mode[]){
  int i = 0;
  while(i<num){
    leds[i] = mode[i];
    i++;
  }
} 
